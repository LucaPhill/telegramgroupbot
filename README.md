#Simple Telegram FAQ Bot  
  
Made with telepot
  
##Description
  
This Bot is created to answer custom answers to custom /commands!  
Every /command can be setup via Private Chat with the bot with no need to write code!  
  
This bot is created to prevent User spam in large groups and will answer your /commands only if:  
  
 - */custom_command* is called by a group Admin in a Group Chat or in a Private Chat  
 - */custom_command* is called by a non Admin in a Private Chat 
 - */default_command* is called by a group Admin in a Private Chat
  
##Setup  
  
For this bot to work are 2 parameter to setup, both can be found in *settings.py*:  
  
 - *GROUP_NAME* = "@group_name"
 - *TOKEN* = "your_token_code"    
  
On how to get a token take a look at: Chapter 6. BotFather https://core.telegram.org/bots  
  
And you must install *telepot* in your python environment, with the command (Windows):
  
 - pip install telepot  
  
Then start *start.py* to launch the Bot  
  
Every *non default* /command will be saved locally in a JSON file called *commands.json* in the subdirectory *commands*.  
Is possible to modify the JSON file Path and Name in *managers\json_manager.py*:  
  
 - *JSON_SUBDIR* = "\\json_file_subdirectory\\"
 - *JSON_NAME* = "your_file_name"    
  
Examples of the format for each type of media saved in *commands.json* can be found at the end of this README
  
##Default Commands
  
From the start there are 6 available /commands  
Every command can be called clicking on a menu button or entering its own /command:
  
 - **Help** ->           */help*:            Show a menu with options:  
    - **Add a command** ->  */add_command*:     Add a new /command, every command will have a *description*  
    - **Modify command** -> */modify_command*:  Modify the *answer* of an existing /command  
    - **Remove command** -> */remove_command*:  Remove a custom /command
    - **Show commands** ->  */custom_commands*: Show all non standard commands added  
 - **Cancel operation** -> */cancel*: Cancel an operation process
   
###/help
  
Provides a brief description of commands access and four in line buttons:
  
  - **Add a command** - Admins only
  - **Modify command** - Admins only
  - **Remove command** - Admins only
  - **Show commands** - Admins and Users

Clicking on one of them will start a new process  
  
###/add_command
     
Start the process to add a new command in 2 steps:

1)  Add a command name and a description  
2)  Add an answer  

In step 1) the Bot asks for a command name and description in this format:  
    *command_name* - *command description*  
  
Remember to put *whitespace-whitespace* between them  
A command name can't contain blank spaces or new lines and a command description can't contain new lines  
  
In step 2) the Bot asks for the content to be showed up when the /command_name is typed  
Almost every format can be attached here(but Telegram game): text/photo/voice/document/music/photo/etc...  


At each step you can cancel the operation simply by clicking on the button **Cancel operation**    
At the end of the operation a **Help** button will be showed  

###/modify_command

Start the process to modify an existing command in 2 steps:  
If no custom command has been added the Bot will reply with *'No command added yet...'*  
  

1)  Type a command name to be modified  
2)  Add a new answer  

In step 1) the Bot asks for a command name to be modified and provide a list of every custom command already added   
If you try to remove a non existing command an error will occur and the Bot will warn you with:
'Command not found!'   
  
In step 2) the Bot asks for the new content to be showed up when the /command_name is typed  
Almost every format can be attached here (but Telegram game): text/photo/voice/document/music/photo/etc...  
This operation cause a timeout to start: after 25 minutes(can be modified) the modifying process is automatically  
cancelled to prevent locking out definitively other users and admins to use the /command if the process won't be not  
terminated   

It's not possible to modify a command that is already been modified by another user, this will cause an error message  
and the Bot will warn you with:  
    *'Command __name_of_the_command__ is already being modified, please wait...*  

When a command is being modified and a user or an admin tries to call it the bot will answer with:   
    *'Command __name_of_the_command__ is being modified, please wait...'*  

At each step you can cancel the operation simply by clicking on the button **Cancel operation**  
At the end of the operation a **Help** button will be showed  
     
###/remove_command

Start the process to remove an existing command in 1 step:  
If no custom command has been added the Bot will reply with *'No command added yet...'*  

1)  Type the command name to be removed  

In step 1) the Bot asks for a command name to be removed and provide a list of every custom command already added   
If you try to remove a non existing command an error will occur and the Bot will warn you with:  
    *'Command not found!'*  

At each step you can cancel the operation simply by clicking on the button **Cancel operation**    
At the end of the operation a **Help** button will be showed  
        
###/custom_commands  

Show every available custom command  
If no command has been added the Bot will answer with:     
    *'No command added yet...'*     
   
##Additional informations

This bot will spawn a new Delegate and a DelegateHandler for each chat conversation as a Thread to provide  
parallel command execution when multiple Admins are managing custom commands  

Each Delegate is provided with a timeout of 30 minutes that will despawn the Delegate after 30 minutes of inactivity  
Each DelegateHandler will provide a Timer object to manage parallel Modify execution 
 
The Timer's timeout have to always be (by default is 25 minutes) < of the Delegate timeout to prevent unexpected   
behaviours to happen if a Delegate goes in timeout when modifying a /command  

##Format supported

This bot supports every media type, but Telegram Games, to be attached as a /command answer  
Every media type is saved in its own format in *commands.json*  

**Examples:**

 - Text:

         {
            "commands": [
                {
                    "command": "/text",
                    "description": "a text message",
                    "answer": {
                        "text": "some text"
                    }
                },
                ...
            ]    
        } 
        
 - Photo:  
    
        {
            "commands": [
                {
                    "command": "/photo",
                    "description": "a photo with caption",
                    "answer": {
                        "photo": "id",
                        "caption": "caption"
                    }
                },
                ...
            ]    
        }           
    
 - Document:
        
        {
            "commands": [
                {
                    "command": "/document",
                    "description": "a document",
                    "answer": {
                        "document": "id",
                        "caption": ""
                    }
                },
                ...
            ]   
        }       
        
 - Video:
    
        {
            "commands": [
                {
                    "command": "/video",
                    "description": "a video",
                    "answer": {
                        "video": "id",
                        "caption": ""
                    }
                },
                ...
            ]    
        }  
    
 - Voice:

        {
            "commands": [
                {
                    "command": "/voice",
                    "description": "a voice file",
                    "answer": {
                        "voice": "id",
                        "caption": ""
                    }
                },
                ...
            ]    
        }   

 - Video Note:
    
        {
            "commands": [
                {
                    "command": "/video_note",
                    "description": "a video note",
                    "answer": {
                        "video_note": "id"
                    }
                },
                ...
            ]    
        }       
      
 - Sticker:
        
        {
            "commands": [
                {
                    "command": "/sticker",
                    "description": "a sticker",
                    "answer": {
                        "sticker": "id"
                    }
                },
                ...
            ]    
        }  
    
 - Animation (GIFs are saved as document):
    
        {
            "commands": [
                {
                    "command": "/animation",
                    "description": "an animation",
                    "answer": {
                        "document": "id",
                        "caption": ""
                    }
                },
                ...
            ]    
        }       
  
 - Location:
    
        {
            "commands": [
                {
                    "command": "/location",
                    "description": "a location",
                    "answer": {
                        "location": {
                            "latitude": 1234,
                            "longitude": 1234,
                            "date": 1234
                        }
                    }
                },
                ...
            ]    
        }
    
 - Audio:
        
        {
            "commands": [
                {
                    "command": "/audio",
                    "description": "an audio",
                    "answer": {
                        "audio": "id",
                        "title": "title",
                        "performer": "performer",
                        "duration": 0,
                        "caption": ""
                    }
                },
                ...
            ]    
        }  
        
 - Media Group (Up to 9 photo and video):
   
        {
            "commands": [
                {
                    "command": "/media_group",
                    "description": "a media group",
                    "answer": {
                        "media_group": "media_group_id",
                        "media": [
                            {
                                "type": "photo",
                                "media": "id"
                            },
                            {
                                "type": "video",
                                "media": "id"
                            },
                            {
                                "type": "photo",
                                "media": "id"
                            },
                            {
                                "type": "video",
                                "media": "id"
                            },
                            ....
                        ]
                    }
                },
                ...
            ]
        }
