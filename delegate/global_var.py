from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from managers.json import get_commands_from_json

bot = ''

admins = []

commands = get_commands_from_json()

CHAT_ID = ''

BOT_NAME = ''

help_keyboard = keyboard = InlineKeyboardMarkup(inline_keyboard=[[
            InlineKeyboardButton(text='◻  Help', callback_data='/help'),
        ]])

cancel_keyboard = InlineKeyboardMarkup(inline_keyboard=[[
        InlineKeyboardButton(text='✖ Cancel operation', callback_data='/cancel'),
    ]])
