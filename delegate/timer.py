import time
from threading import Thread

import delegate.global_var as global_var


class Timer(Thread):

    def __init__(self, timeout=5):
        super(Timer, self).__init__()
        self.timeout = timeout
        self.running = False
        self.temp = []
        self.count = 0

    def run(self):
        while True:
            while self.running and self.count < self.timeout:
                time.sleep(1)
                self.count += 1
            if self.count == self.timeout:
                self.running = False
                self.count = 0
                self._update_command()
            time.sleep(2)

    def start_timer(self, command):
        self.temp.append(command)
        self.running = True

    def stop_timer(self):
        self.running = False
        self.count = 0
        self.temp = []

    def _update_command(self):
        command_name = self.temp[0][0]
        for command in global_var.commands:
            if command[0] == command_name or command[0] == '/' + command_name:
                global_var.commands.remove(command)
                global_var.commands.append(self.temp[0])
                self.temp = []
