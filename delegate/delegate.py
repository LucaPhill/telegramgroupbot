import telepot

from delegate import global_var
from delegate.timer import Timer
from managers import (audio, contact, document, game, location,
                      photo, sticker, text, video, video_note, voice)


class DelegateHandler(telepot.helper.ChatHandler):

    def __init__(self, *args, **kwargs):
        super(DelegateHandler, self).__init__(*args, **kwargs)
        self.temp_command = []
        self.timer = Timer(timeout=60 * 25)
        self.timer.start()
        self._settings = {
            "command_to_be_modified": "",
            "wait_first_add_reply": False,
            "wait_second_add_reply": False,
            "wait_first_modify_reply": False,
            "wait_second_modify_reply": False,
            "wait_remove_reply": False
        }

    # Message handler
    def on_chat_message(self, msg):
        content_type = telepot.glance(msg)[0]

        if content_type == 'photo':
            photo.manage(
                message=msg,
                delegate_settings=self._settings,
                temporary_commands=self.temp_command,
                timer=self.timer
            )
        elif content_type == 'document':
            document.manage(
                message=msg,
                delegate_settings=self._settings,
                temporary_commands=self.temp_command,
                timer=self.timer
            )
        elif content_type == 'video':
            video.manage(
                message=msg,
                delegate_settings=self._settings,
                temporary_commands=self.temp_command,
                timer=self.timer
            )
        elif content_type == 'voice':
            voice.manage(
                message=msg,
                delegate_settings=self._settings,
                temporary_commands=self.temp_command,
                timer=self.timer
            )
        elif content_type == 'location':
            location.manage(
                message=msg,
                delegate_settings=self._settings,
                temporary_commands=self.temp_command,
                timer=self.timer
            )
        elif content_type == 'sticker':
            sticker.manage(
                message=msg,
                delegate_settings=self._settings,
                temporary_commands=self.temp_command,
                timer=self.timer
            )
        elif content_type == 'audio':
            audio.manage(
                message=msg,
                delegate_settings=self._settings,
                temporary_commands=self.temp_command,
                timer=self.timer
            )
        elif content_type == 'contact':
            contact.manage(
                message=msg,
                delegate_settings=self._settings,
                temporary_commands=self.temp_command,
                timer=self.timer
            )
        elif content_type == 'video_note':
            video_note.manage(
                message=msg,
                delegate_settings=self._settings,
                temporary_commands=self.temp_command,
                timer=self.timer
            )
        elif content_type == 'text':
            text.manage(
                message=msg,
                delegate_settings=self._settings,
                temporary_commands=self.temp_command,
                timer=self.timer
            )
        elif content_type == 'game':
            game.manage(
                message=msg,
                delegate_settings=self._settings
            )

    # Callback query handler
    def on_callback_query(self, msg):
        # Get message information
        query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
        chat_type = msg['message']['chat']['type']
        chat_id = msg['message']['chat']['id']
        message_id = msg['message']['message_id']
        user_id = msg['from']['username']

        # Admins can add, modify, remove commands in a private chat
        if user_id in global_var.admins and chat_type == 'private':
            text.admins_commands(query_data, chat_id, self._settings, self.temp_command, self.timer)

        if user_id in global_var.admins and chat_id == global_var.CHAT_ID or chat_type == 'private':
            text.users_and_admins_commands(query_data, chat_id, message_id)
