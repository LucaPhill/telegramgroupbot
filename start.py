import time

from telepot import DelegatorBot
from telepot.delegate import pave_event_space, per_chat_id, create_open, include_callback_query_chat_id
from telepot.loop import MessageLoop

import settings
import delegate.delegate as delegate
import delegate.global_var as global_var


# 30 minutes
timeout = 60 * 30

# Spawn a delegate bot for each chat

global_var.bot = DelegatorBot(settings.TOKEN, [include_callback_query_chat_id(pave_event_space())(
            per_chat_id(), create_open, delegate.DelegateHandler, timeout=timeout)])

global_var.CHAT_ID = global_var.bot.getChat(settings.GROUP_NAME)['id']
global_var.BOT_NAME = '@' + global_var.bot.getMe()['username']


# Callback function on message received
MessageLoop(global_var.bot).run_as_thread()
print('Listening...')


# Return Admins username
def get_admins(chat_id):
    group_admins = ['']
    response = global_var.bot.getChatAdministrators(chat_id)
    for admin in response:
        u = admin.get('user').get('username')
        group_admins.append(u)
    return group_admins


# Keep the bot running and update Admin every 1 hour
while True:
    global_var.admins = get_admins(settings.GROUP_NAME)
    time.sleep(60 * 60)
