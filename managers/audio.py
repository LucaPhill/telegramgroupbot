import telepot

import delegate.global_var as global_var
import managers.functions as function


# Audio message type manager
def manage(message, delegate_settings, temporary_commands, timer):
    # Get message information
    content_type, chat_type, chat_id = telepot.glance(message)
    user_id = message['from']['username']

    # Only Admins can add, modify or remove commands in a private chat with the bot
    if user_id in global_var.admins and chat_type == 'private':
        audio = _get_audio_id_title_performer_duration_caption(message)

        if delegate_settings['wait_second_add_reply']:
            function.second_add(audio, chat_id, temporary_commands)
            delegate_settings['wait_second_add_reply'] = False

        elif delegate_settings['wait_second_modify_reply']:
            function.second_modify(
                audio, chat_id, delegate_settings['command_to_be_modified'], temporary_commands, timer
            )
            delegate_settings['command_to_be_modified'] = ''
            delegate_settings['wait_second_modify_reply'] = False


# Get audio file ID,
def _get_audio_id_title_performer_duration_caption(message):
    if 'caption' in message:
        return_message = {
            'audio': message['audio']['file_id'],
            'title': message['audio']['title'],
            'performer': message['audio']['performer'],
            'duration': message['audio']['duration'],
            'caption': message['caption']
        }
    else:
        return_message = {
            'audio': message['audio']['file_id'],
            'title': message['audio']['title'],
            'performer': message['audio']['performer'],
            'duration': message['audio']['duration'],
            'caption': ''
        }
    return return_message
