import telepot
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

import delegate.global_var as global_var
import managers.functions as function


# Text message type manager
def manage(message, delegate_settings, temporary_commands, timer):

    # Get message information
    content_type, chat_type, chat_id = telepot.glance(message)
    message_id = message['message_id']
    user_id = message['from']['username']

    # Admins can add, modify, remove commands in a private chat
    if user_id in global_var.admins and chat_type == 'private':
        text = message['text']
        if admins_commands(text, chat_id, delegate_settings, temporary_commands, timer):
            return

    # Admins can use commands in their group
    # Users can use commands in a private chat
    if user_id in global_var.admins and chat_id == global_var.CHAT_ID or chat_type == 'private':
        text = message['text']
        users_and_admins_commands(text, chat_id, message_id)


def users_and_admins_commands(text, chat_id, message_id):
    if text == '/custom_commands' or text == '/custom_commands' + global_var.BOT_NAME:
        response = _get_custom_commands_name_as_string()
        global_var.bot.sendMessage(chat_id, response, reply_to_message_id=message_id)

    elif text == '/help' or text == '/help' + global_var.BOT_NAME:
        _get_help_answer(chat_id)

    else:
        # Return a command answer
        for command, description, answer in global_var.commands:
            if text == command or text == command + global_var.BOT_NAME:
                _get_answer(chat_id, answer, message_id)


# Methods to add, modify, remove a command
def admins_commands(text, chat_id, delegate_settings, temp, timer):

    if text == '/add_command' or text == '/add_command' + global_var.BOT_NAME:
        _add1(chat_id, delegate_settings)
        return True

    elif delegate_settings['wait_first_add_reply']:
        _add2(text, chat_id, delegate_settings, temp)
        return True

    elif delegate_settings['wait_second_add_reply']:
        _add3(text, chat_id, delegate_settings, temp)
        return True

    elif text == '/modify_command' or text == '/modify_command' + global_var.BOT_NAME:
        _modify1(chat_id, delegate_settings)
        return True

    elif delegate_settings['wait_first_modify_reply']:
        _modify2(text, chat_id, delegate_settings, temp, timer)
        return True

    elif delegate_settings['wait_second_modify_reply']:
        _modify3(text, chat_id, delegate_settings, temp, timer)
        return True

    elif text == '/remove_command' or text == '/remove_command' + global_var.BOT_NAME:
        _remove1(chat_id, delegate_settings)
        return True

    elif delegate_settings['wait_remove_reply']:
        _remove2(text, chat_id, delegate_settings)
        return True

    return False


def _get_help_answer(chat_id):
    keyboard = InlineKeyboardMarkup(inline_keyboard=[
        [
            InlineKeyboardButton(text='🅰   Add  a  command', callback_data='/add_command'),
            InlineKeyboardButton(text='🅰 Modify command', callback_data='/modify_command'),
        ],
        [
            InlineKeyboardButton(text='🅰 Remove command', callback_data='/remove_command'),
            InlineKeyboardButton(text='✅  Show commands', callback_data='/custom_commands'),
        ],
    ])
    response = '✅ Available to Users and Admins \n🅰 Available to Admins only'
    global_var.bot.sendMessage(chat_id, response, reply_markup=keyboard)


def _get_answer(chat_id, answer, message_id):
    answer_type = list(answer.keys())[0]

    if answer_type == 'text':
        global_var.bot.sendMessage(
            chat_id=chat_id,
            text=answer['text'],
            reply_to_message_id=message_id
        )
    elif answer_type == 'photo':
        global_var.bot.sendPhoto(
            chat_id=chat_id,
            photo=answer['photo'],
            caption=answer['caption'],
            reply_to_message_id=message_id
        )
    elif answer_type == 'document':
        global_var.bot.sendDocument(
            chat_id=chat_id,
            document=answer['document'],
            caption=answer['caption'],
            reply_to_message_id=message_id
        )
    elif answer_type == 'video':
        global_var.bot.sendVideo(
            chat_id=chat_id,
            video=answer['video'],
            caption=answer['caption'],
            reply_to_message_id=message_id
        )
    elif answer_type == 'voice':
        global_var.bot.sendVoice(
            chat_id=chat_id,
            voice=answer['voice'],
            caption=answer['caption'],
            reply_to_message_id=message_id
        )
    elif answer_type == 'location':
        global_var.bot.sendLocation(
            chat_id=chat_id,
            latitude=answer['location']['latitude'],
            longitude=answer['location']['longitude'],
            reply_to_message_id=message_id
        )
    elif answer_type == 'sticker':
        global_var.bot.sendSticker(
            chat_id=chat_id,
            sticker=answer['sticker'],
            reply_to_message_id=message_id
        )
    elif answer_type == 'audio':
        global_var.bot.sendAudio(
            chat_id=chat_id,
            audio=answer['audio'],
            title=answer['title'],
            performer=answer['performer'],
            duration=answer['duration'],
            caption=answer['caption'],
            reply_to_message_id=message_id
        )
    elif answer_type == 'contact':
        global_var.bot.sendContact(
            chat_id=chat_id,
            phone_number=answer['contact'],
            first_name=answer['first_name'],
            last_name=answer['last_name'],
            reply_to_message_id=message_id
        )
    elif answer_type == 'video_note':
        global_var.bot.sendVideoNote(
            chat_id=chat_id,
            video_note=answer['video_note'],
            reply_to_message_id=message_id
        )
    elif answer_type == 'media_group':
        global_var.bot.sendMediaGroup(
            chat_id=chat_id,
            media=answer['media'],
            reply_to_message_id=message_id
        )


def _get_custom_commands_name_as_string():
    response = ''
    for command, description, answer in global_var.commands:
        response += '\n%s - %s' % (command, description)
    if response == '':
        response = 'No command added yet...'
    return response


def _add1(chat_id, delegate_settings):
    response = 'Insert a new command with this format: \ncommand_name - A brief description'
    global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.cancel_keyboard)
    delegate_settings['wait_first_add_reply'] = True


def _add2(text, chat_id, delegate_settings, temp):
    delegate_settings['wait_second_add_reply'] = function.add(text, chat_id, temp)
    delegate_settings['wait_first_add_reply'] = False


def _add3(text, chat_id, delegate_settings, temp):
    content = {'text': text}
    function.second_add(content, chat_id, temp)
    delegate_settings['wait_second_add_reply'] = False


def _modify1(chat_id, delegate_settings):
    commands = _get_custom_commands_name_as_string()
    if commands == 'No command added yet...':
        global_var.bot.sendMessage(chat_id, commands)
    else:
        response = 'Click on a command name or type it to modify:' + commands
        global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.cancel_keyboard)
        delegate_settings['wait_first_modify_reply'] = True


def _modify2(text, chat_id, delegate_settings, temp, timer):
    wait_second_modify_reply, command_to_be_modified = function.modify(text, chat_id, temp, timer)
    delegate_settings['wait_second_modify_reply'] = wait_second_modify_reply
    delegate_settings['command_to_be_modified'] = command_to_be_modified
    delegate_settings['wait_first_modify_reply'] = False


def _modify3(text, chat_id, delegate_settings, temp, timer):
    content = {'text': text}
    function.second_modify(content, chat_id, delegate_settings['command_to_be_modified'], temp, timer)
    delegate_settings['command_to_be_modified'] = ''
    delegate_settings['wait_second_modify_reply'] = False


def _remove1(chat_id, delegate_settings):
    response = 'Click on a command name or type it to remove:\n'
    response += _get_custom_commands_name_as_string()
    global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.cancel_keyboard)
    delegate_settings['wait_remove_reply'] = True


def _remove2(text, chat_id, delegate_settings):
    function.remove(text, chat_id)
    delegate_settings['wait_remove_reply'] = False
