import telepot

from managers import media_group
import delegate.global_var as global_var
import managers.functions as function


# Photo message type manager
def manage(message, delegate_settings, temporary_commands, timer):
    # Get message information
    content_type, chat_type, chat_id = telepot.glance(message)
    user_id = message['from']['username']

    # Only Admins can add, modify or remove commands in a private chat with the bot
    if user_id in global_var.admins and chat_type == 'private':
        if 'media_group_id' in message:
            media_group.manage(message, delegate_settings, temporary_commands, timer)
        else:
            video = _get_video_and_caption(message)

            if delegate_settings['wait_second_add_reply']:
                function.second_add(video, chat_id, temporary_commands)
                delegate_settings['wait_second_add_reply'] = False

            elif delegate_settings['wait_second_modify_reply']:
                function.second_modify(
                    video, chat_id, delegate_settings['command_to_be_modified'], temporary_commands, timer
                )
                delegate_settings['command_to_be_modified'] = ''
                delegate_settings['wait_second_modify_reply'] = False


# Get document file ID and caption to save
def _get_video_and_caption(message):
    if 'caption' in message:
        return_message = {
            'video': message['video']['file_id'],
            'caption': message['caption']}
    else:
        return_message = {
            'video': message['video']['file_id'],
            'caption': ''}
    return return_message
