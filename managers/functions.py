from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

import delegate.global_var as global_var
from managers.json import override_json


# Add

# Add a new command name and description and override json file to save changes
# Backup and restore command list in case of unexpected failure
def add(text, chat_id, temp):
    new_command, new_description = _text_split(text, chat_id)

    if _text_check(new_command, new_description, chat_id):
        temp.append(('/' + new_command, new_description, ''))
        override_json(global_var.commands)
        response = ('Now tell me the answer I will send on command call...\n'
                    'Type it, send me a file or drag and drop anything!')
        global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.cancel_keyboard)
        return True
    else:
        return False


def _text_split(text, chat_id):
    if text == '/cancel':
        response = 'Add cancelled!'
        global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.help_keyboard)
        return '', ''
    try:
        new_command, new_description = text.split(' - ')
        return new_command, new_description

    except Exception as e:
        print(str(e) + ', message causing exception: %s' % text)
        response = 'New command format is wrong...'
        keyboard = InlineKeyboardMarkup(inline_keyboard=[[
            InlineKeyboardButton(text='↪  Retry', callback_data='/add_command'),
        ]])
        global_var.bot.sendMessage(chat_id, response, reply_markup=keyboard)
        return '', ''


def _text_check(new_command, new_description, chat_id):
    keyboard = InlineKeyboardMarkup(inline_keyboard=[[
        InlineKeyboardButton(text='↪  Retry', callback_data='/add_command'),
    ]])

    if '\n' in new_command:
        response = 'Command name cannot contain new_line, please retry...'
        global_var.bot.sendMessage(chat_id, response, reply_markup=keyboard)
        return False

    elif '\n' in new_description:
        response = 'Command description cannot contain new_line, please retry...'
        global_var.bot.sendMessage(chat_id, response, reply_markup=keyboard)
        return False

    elif ' ' in new_command:
        response = 'Command name cannot contain whitespace, please retry...'
        global_var.bot.sendMessage(chat_id, response, reply_markup=keyboard)
        return False

    elif new_command == '' or new_description == '':
        return False

    return True


# Add new command answer and override json file to save changes
def second_add(text, chat_id, temp):
    if 'text' in text:
        if text['text'] == '/cancel':
            response = 'Add cancelled!'
            temp.remove(temp[0])
            global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.help_keyboard)
            return

    _add(temp, text)
    override_json(global_var.commands)
    response = 'Command added!'
    global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.help_keyboard)


def _add(temp, text):
    new_command = (temp[0][0], temp[0][1], text)
    global_var.commands.append(new_command)
    temp.remove(temp[0])


# Modify

# Search for command name to modify
def modify(text, chat_id, temp, timer):
    for command in global_var.commands:
        if command[0] == text or command[0] == '/' + text:

            if _is_being_modified(command):
                response = 'Command %s is already being modified, please wait...' % command[0]
                global_var.bot.sendMessage(chat_id, response)
                return False, ''
            else:
                response = ('Command found!\n'
                            'Now tell me the answer I will send on command call...\n'
                            'Type it, send me a file or drag and drop anything!')
                _modify_start(temp, command)
                timer.start_timer(command)
                global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.cancel_keyboard)
                return True, text

    if text == '/cancel':
        response = 'Modify cancelled'
        global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.help_keyboard)
    else:
        response = 'Command not found!'
        keyboard = InlineKeyboardMarkup(inline_keyboard=[[
            InlineKeyboardButton(text='↪  Retry', callback_data='/modify_command'),
        ]])
        global_var.bot.sendMessage(chat_id, response, reply_markup=keyboard)
    return False, ''


def _is_being_modified(command):
    check_answer = {'text': 'Command %s is being modified, please wait...' % command[0]}
    if command[2] == check_answer:
        return True
    return False


def _modify_start(temp, command):
    temp.append(command)
    global_var.commands.remove(command)
    temp_answer = {'text': 'Command %s is being modified, please wait...' % command[0]}
    temp_command = (command[0], command[1], temp_answer)
    global_var.commands.append(temp_command)
    override_json(global_var.commands)


# Modify a command answer and override json file to save changes
# Backup and restore command list in case of unexpected failure
def second_modify(text, chat_id, command_name, temp, timer):

    if timer.running:
        if 'text' in text:
            if text['text'] == '/cancel':
                _restore_command(temp, command_name)
                override_json(global_var.commands)
                timer.stop_timer()
                response = 'Modify cancelled'
                global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.help_keyboard)
                return

        _modify_add(text, temp, command_name)
        override_json(global_var.commands)
        timer.stop_timer()
        response = 'Command modified!'
    else:
        response = 'Modify timed out!'
    global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.help_keyboard)


def _restore_command(temp, command_name):
    for command in global_var.commands:
        if command[0] == command_name or command[0] == '/' + command_name:
            global_var.commands.remove(command)
            global_var.commands.append(temp[0])
            temp.remove(temp[0])


def _modify_add(text, temp, command_name):
    new_command = (temp[0][0], temp[0][1], text)
    for command in global_var.commands:
        if command[0] == command_name or command[0] == '/' + command_name:
            global_var.commands.remove(command)
            global_var.commands.append(new_command)
            temp.remove(temp[0])


# Remove

# Remove a command and override json file to save changes
# Backup and restore command list in case of unexpected failure
def remove(text, chat_id):
    if _found_and_removed(text):
        response = 'Command %s removed!' % text
        global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.help_keyboard)

    elif text == '/cancel':
        response = 'Remove cancelled!'
        global_var.bot.sendMessage(chat_id, response, reply_markup=global_var.help_keyboard)
    else:
        response = 'Command not found!'
        keyboard = InlineKeyboardMarkup(inline_keyboard=[[
            InlineKeyboardButton(text='↪  Retry', callback_data='/remove_command'),
        ]])
        global_var.bot.sendMessage(chat_id, response, reply_markup=keyboard)


def _found_and_removed(text):
    for command in global_var.commands:
        if command[0] == text or command[0] == '/' + text:
            global_var.commands.remove(command)
            override_json(global_var.commands)
            return True
    return False
