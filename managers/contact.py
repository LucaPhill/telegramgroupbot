import telepot

import delegate.global_var as global_var
import managers.functions as function


# Contact message type manager
def manage(message, delegate_settings, temporary_commands, timer):
    # Get message information
    content_type, chat_type, chat_id = telepot.glance(message)
    user_id = message['from']['username']

    # Only Admins can add, modify or remove commands in a private chat with the bot
    if user_id in global_var.admins and chat_type == 'private':
        contact = _get_contact_first_name_and_last_name(message)

        if delegate_settings['wait_second_add_reply']:
            function.second_add(contact, chat_id, temporary_commands)
            delegate_settings['wait_second_add_reply'] = False

        elif delegate_settings['wait_second_modify_reply']:
            function.second_modify(
                contact, chat_id, delegate_settings['command_to_be_modified'], temporary_commands, timer
            )
            delegate_settings['command_to_be_modified'] = ''
            delegate_settings['wait_second_modify_reply'] = False


# Get contact file ID and caption to save
def _get_contact_first_name_and_last_name(message):
    if 'last_name' in message['contact']:
        return_message = {
            'contact': message['contact']['phone_number'],
            'first_name': message['contact']['first_name'],
            'last_name': message['contact']['last_name']
        }
    else:
        return_message = {
            'contact': message['contact']['phone_number'],
            'first_name': message['contact']['first_name'],
            'last_name': ''
        }
    return return_message
