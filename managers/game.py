import telepot

import delegate.global_var as global_var


# Game message type manager
def manage(message, delegate_settings):
    # Get message information
    content_type, chat_type, chat_id = telepot.glance(message)
    user_id = message['from']['username']

    # Only Admins can add, modify or remove commands in a private chat with the bot
    if user_id in global_var.admins and chat_type == 'private':
        if _waiting_for_answer(delegate_settings):
                response = ('Sorry, unfortunately i can\'t send a game!\n'
                            'Please, tell me the answer I will send on command call...\n'
                            'Type it, send me a file or drag and drop anything, but a game!')
                global_var.bot.sendMessage(chat_id, response)


def _waiting_for_answer(delegate_settings):
    if delegate_settings['wait_second_add_reply']:
        return True
    elif delegate_settings['wait_second_modify_reply']:
        return True
    return False
