import telepot

import delegate.global_var as global_var
import managers.functions as function


# Document message type manager
def manage(message, delegate_settings, temporary_commands, timer):
    # Get message information
    content_type, chat_type, chat_id = telepot.glance(message)
    user_id = message['from']['username']

    # Only Admins can add, modify or remove commands in a private chat with the bot
    if user_id in global_var.admins and chat_type == 'private':
        document = _get_document_and_caption(message)

        if delegate_settings['wait_second_add_reply']:
            function.second_add(document, chat_id, temporary_commands)
            delegate_settings['wait_second_add_reply'] = False

        elif delegate_settings['wait_second_modify_reply']:
            function.second_modify(
                document, chat_id, delegate_settings['command_to_be_modified'], temporary_commands, timer
            )
            delegate_settings['command_to_be_modified'] = ''
            delegate_settings['wait_second_modify_reply'] = False


# Get document file ID and caption to save
def _get_document_and_caption(message):
    if 'caption' in message:
        return_message = {
            'document': message['document']['file_id'],
            'caption': message['caption']
        }
    else:
        return_message = {
            'document': message['document']['file_id'],
            'caption': ''
        }
    return return_message
