import telepot

from managers.json import override_json
import delegate.global_var as global_var
import managers.functions as function


# Location message type manager
def manage(message, delegate_settings, temporary_commands, timer):
    # Get message information
    content_type, chat_type, chat_id = telepot.glance(message)
    user_id = message['from']['username']
    date = message['date']
    location = _get_latitude_and_longitude(message)

    # Check if location have to be updated
    if 'edit_date' in message:
        command = _get_command(date)
        # Update real time location if needed
        if command != '':
            _update_location(command, location)

    # Only Admins can add, modify or remove commands in a private chat with the bot
    if user_id in global_var.admins and chat_type == 'private':
        if delegate_settings['wait_second_add_reply']:
            function.second_add(location, chat_id, temporary_commands)
            delegate_settings['wait_second_add_reply'] = False

        elif delegate_settings['wait_second_modify_reply']:
            function.second_modify(
                location, chat_id, delegate_settings['command_to_be_modified'], temporary_commands, timer
            )
            delegate_settings['command_to_be_modified'] = ''
            delegate_settings['wait_second_modify_reply'] = False


# Return the command that shows a real time shared location
def _get_command(date):
    for command, description, answer in global_var.commands:
        if 'location' in answer:
            if answer['location']['date'] == date:
                return command
    return ''


# Get location file latitude, longitude and date to save
def _get_latitude_and_longitude(message):
    return_message = {
            'location': {
                'latitude': message['location']['latitude'],
                'longitude': message['location']['longitude'],
                'date': message['date']
            }
    }
    return return_message


# Update an existing location shared in real time
def _update_location(cmd, location):
    for command in global_var.commands:
        if command[0] == cmd:
            new_command = (command[0], command[1], location)
            global_var.commands.remove(command)
            global_var.commands.append(new_command)
    override_json(global_var.commands)
