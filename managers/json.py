import json
import os


DIV = os.sep
# JSON_SUBDIR and JSON_NAME can be changed accordingly to user preferences
# use DIV as separator between folder\file
JSON_SUBDIR = DIV + 'commands' + DIV
JSON_NAME = 'commands.json'

JSON_PATH = os.getcwd() + JSON_SUBDIR + JSON_NAME


# Initialize commands from JSON file
def get_commands_from_json():
    json_commands = []
    try:
        with open(JSON_PATH) as f:
            file = json.load(f)
        for element in file.get('commands'):
            command = element['command']
            description = element['description']
            answer = element['answer']
            json_commands.append((command, description, answer))
        return json_commands
    except Exception as e:
        print(str(e))
        return json_commands


# Override JSON file with the new command list
# Return True if success, False if catch an error
def override_json(commands_list):
    try:
        data = _format_command_list(commands_list)
        with open(JSON_PATH, 'w') as f:
            json.dump(data, f, indent=4)
    except Exception as e:
        print(str(e))
        raise


def _format_command_list(commands_list):
    data = {'commands': []}
    for command in commands_list:
        data['commands'].append({
            'command': command[0],
            'description': command[1],
            'answer': command[2]})
    return data
