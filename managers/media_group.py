import telepot

from managers.json import override_json
import delegate.global_var as global_var
import managers.functions as function


# Video note message type manager
def manage(message, delegate_settings, temporary_commands, timer):
    # Get message information
    content_type, chat_type, chat_id = telepot.glance(message)
    user_id = message['from']['username']

    # Only Admins can add, modify or remove commands in a private chat with the bot
    if user_id in global_var.admins and chat_type == 'private':
        media_group = _update_media_group(message)

        if delegate_settings['wait_second_add_reply']:
            function.second_add(media_group, chat_id, temporary_commands)
            delegate_settings['wait_second_add_reply'] = False

        elif delegate_settings['wait_second_modify_reply']:
            function.second_modify(
                media_group, chat_id, delegate_settings['command_to_be_modified'], temporary_commands, timer
            )
            delegate_settings['command_to_be_modified'] = ''
            delegate_settings['wait_second_modify_reply'] = False
        else:
            _modify_media_group(media_group)


def _get_id_and_type(message):
    return_message = ''
    if 'photo' in message:
        return_message = {
            'type': 'photo',
            'media': message['photo'][-1]['file_id']
        }
    elif 'video' in message:
        return_message = {
            'type': 'video',
            'media': message['video']['file_id']
        }
    return return_message


def _is_new_and_media(message, media_group_id):
    for command, description, answer in global_var.commands:
        if 'media_group' in answer and media_group_id == answer['media_group']:
            media = answer['media']
            media.append(_get_id_and_type(message))
            return False, media
    return True, []


def _update_media_group(message):
    media_group_id = message['media_group_id']
    is_new, media = _is_new_and_media(message, media_group_id)

    if is_new:
        media.append(_get_id_and_type(message))
    return_message = {
        'media_group': media_group_id,
        'media': media
    }
    return return_message


# Search for command name to modify
def _modify_media_group(media_group):
    for command in global_var.commands:
        if 'media_group' in command[2] and media_group['media_group'] == command[2]['media_group']:
                new_command = (command[0], command[1], media_group)
                global_var.commands.remove(command)
                global_var.commands.append(new_command)
                override_json(global_var.commands)
